/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;
import java.util.Scanner;
/**
 *
 * @author andsa
 */

public class TestTerreno {
    
    
    public static void main(String[] args) {

       //declarar variables
       Terreno terreno = new Terreno();
       Scanner sc = new Scanner(System.in);
       int opcion = 0;
       float perimetro = 0.0f, area = 0.0f, ancho = 0.0f, largo = 0.0f;
       
       do { 
           
           System.out.println("1.Iniciar el Objetos");
           System.out.println("2.Cambiar Ancho");
           System.out.println("3.Cambiar Largo");
           System.out.println("4 Mostrar Informacion");
           System.out.println("5.Salir");
           System.out.print("Dame la opcion :");
           opcion = sc.nextInt();
           
           switch(opcion){
               case 1:
                   System.out.println(" Dame lo ancho del terreno: ");
                   ancho = sc.nextFloat();
                   System.out.println("Dame lo largo del terreno: ");
                   largo = sc.nextFloat();
                   terreno.setAncho(ancho);
                   terreno.setLargo(largo);
                   break;
               case 2:
                   System.out.println(" Dame lo ancho del terreno: ");
                   ancho = sc.nextFloat();
                   terreno.setAncho(ancho);
                   break;
               case 3:
                   System.out.println("Dame lo largo del terreno: ");
                   largo = sc.nextFloat();
                   terreno.setLargo(largo);
                   break;
               case 4:
                   terreno.imprimirTerreno();
                   break;
               case 5:
                   System.out.println("Hasta la vista Baby .......!");
                   break;
                   
               default:
                   System.out.println("No es una opcion valida usa 1-5");  
           }
        } while(opcion!=5);
    }
}