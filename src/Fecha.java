/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author andsa
 */
public class Fecha {
    private int dia;
    private int mes;
    private int año;
    
    //Meter constructor por omisión y por argumentos con instert code -> constructor.
    public Fecha() {
         this.dia = 1;
         this.mes = 1;
         this.año = 2000;
    }
    
    public Fecha(int dia, int mes, int año) {
        this.dia = dia;
        this.mes = mes;
        this.año = año;
    }
    
    //set's & get's (w/insert code)

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    //Realizar un método que regrese una variable String con el formato largo de
    //la fecha ej: 4 de Marzo del 2024.
    public String FechaFormatoLargo(){
         String fecha="";
         fecha=fecha+dia+" de ";
         switch(this.mes){
             case 1:
                 fecha+="Enero"; break;
             case 2:
                 fecha+="Febrero"; break;
             case 3:
                 fecha+="Marzo"; break;
             case 4:
                 fecha+="Abril"; break;
             case 5:
                fecha+="Mayo"; break;
             case 6:
                 fecha+="Junio"; break;
             case 7:
                 fecha+="Julio"; break;
             case 8:
                 fecha+="Agosto"; break;
             case 9:
                 fecha+="Septiembre"; break;
             case 10:
                fecha+="Octubre"; break;
             case 11:
                 fecha+="Noviembre"; break;
             case 12:
                 fecha+="Diciembre"; break;
         }
         
         fecha=fecha + " del "+ this.año;
         
         return fecha;        
    }
}
