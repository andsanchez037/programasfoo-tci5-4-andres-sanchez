/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import java.util.logging.Logger;
/**
 *
 * @author andsa
 */
public class Nomina {
    private int numNomina;
    private String nombre;
    private int nivel;
    private int pagoH;
    private int horasT;

public Nomina(){
    
}

public Nomina (int numNomina, String nombre, int nivel, int pagoH, int horasT){
        this.numNomina=numNomina;
        this.nombre=nombre;
        this.nivel=nivel;
        this.pagoH=pagoH;
        this.horasT=horasT;
    }

    public int getNumNomina() {
        return numNomina;
    }

    public void setNumNomina(int numNomina) {
        this.numNomina = numNomina;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPagoH() {
        return pagoH;
    }

    public void setPagoH(int pagoH) {
        this.pagoH = pagoH;
    }

    public int getHorasT() {
        return horasT;
    }

    public void setHorasT(int horasT) {
        this.horasT = horasT;
    }

}
