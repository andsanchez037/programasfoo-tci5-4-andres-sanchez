/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import java.util.logging.Logger;

/**
 *
 * @author andsa
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombCliente;
    private String domFiscal;
    private String desc;
    private String fechaVenta;
    private float totalVenta;
    private double impuesto;
    
    public Factura(){
        this.numFactura =0;
        this.rfc = "";
        this.nombCliente="";
        this.domFiscal="";
        this.desc="";
        this.fechaVenta="";
        this.totalVenta= 0.0f;
    }
    
    public Factura(int numFactura, String rfc, String nombCliente, String domFiscal, String desc, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombCliente = nombCliente;
        this.domFiscal = domFiscal;
        this.desc = desc;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    public Factura(Factura otro){
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombCliente = otro.nombCliente;
        this.domFiscal = otro.domFiscal;
        this.desc = otro.desc;
        this.fechaVenta = otro.fechaVenta;
        this.totalVenta = otro.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombCliente() {
        return nombCliente;
    }

    public void setNombCliente(String nombCliente) {
        this.nombCliente = nombCliente;
    }

    public String getDomFiscal() {
        return domFiscal;
    }

    public void setDomFiscal(String domFiscal) {
        this.domFiscal = domFiscal;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    public float calcularImpuesto(){
        float impuesto =  0.0f;
        impuesto =this.totalVenta * 0.16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total= this.calcularImpuesto() + this.totalVenta;
        return total;
    }
    public void imprimirFactura(){
        System.out.println(" Numero de Factura:  " + this.numFactura);
        System.out.println(" RFC: " + this.rfc);
        System.out.println(" Nombre del Cliente: " + this.nombCliente);
        System.out.println(" Domicilio Fiscal: " + this.domFiscal);
        System.out.println(" Descripcion: " + this.desc);
        System.out.println(" Fecha de la Venta: " + this.fechaVenta);
        System.out.println(" Total de la Venta: " + this.totalVenta);
        System.out.println(" Impuesto: " + this.calcularImpuesto());
        System.out.println(" Total a Pagar: " + this.calcularTotalPagar());
    }   
}